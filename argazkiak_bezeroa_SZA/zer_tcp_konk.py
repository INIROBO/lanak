#!/usr/bin/env python3

import socket, os, signal, cv2, numpy, szasar

PORT = 50004

s = socket.socket( socket.AF_INET, socket.SOCK_STREAM )

s.bind( ('', PORT) )
s.listen( 5 )

signal.signal(signal.SIGCHLD, signal.SIG_IGN)

while True:
    elkarrizketa, bez_helb = s.accept()
    print( "Bezeroa konektatuta {}:{}.".format( bez_helb[0], bez_helb[1] ) )
    if os.fork():
        elkarrizketa.close()
    else:
        s.close()
        imageSize = 0
        # uploa = False
        end = False
        while not end:
            buf = b''
            while True:
                if imageSize > 0:
                    buf = szasar.recvall(elkarrizketa, imageSize)
                    imageSize = 0
                    break
                else:
                    buf += elkarrizketa.recv( 1024 )
                    print(buf)
                    if buf[len(buf)-3:].decode("us-ascii")\
                            == "|\r\n":
                        break
                    if buf == b'':
                        end = True
                        break
            if end:
                continue

            command = buf[:4].decode("us-ascii")
            print(command)
            if command == "UPLO":
                print(buf[4:])
                print(type(buf[4:]), type(buf))
                np_arr = numpy.frombuffer(buf[4:], dtype=numpy.uint8)
                img = cv2.imdecode(np_arr, flags = -1)
                print(img)
                cv2.imwrite("test.jpg", img)

            elif command == "PICT":
                par = buf[4:len(buf)-3].decode()
                print(command, par)
                splitted = par.split('|')
                imageSize = int(splitted[1])
                print(imageSize)
            elif command == "PHOT":
                img_open = cv2.imread("Figure_1.jpg", flags = -1)
                img_str = numpy.array(cv2.imencode('.jpg',\
                                                   img_open)[1]).tostring()

            else:
                print("normal")
                print(command, buf[4:len(buf)-3].decode())

            send = input()
            print("listo")
            if command != "PHOT":
                ready = send[:3].encode("us-ascii") +\
                                    send[3:].encode() +\
                                    "|\r\n".encode("us-ascii")
            else:
                ready = send[:3].encode("us-ascii") +\
                                    str(len(img_str)).encode("us-ascii") +\
                    "|".encode("us-ascii") + img_str
            print(ready[:50])
            elkarrizketa.sendall(ready)
        print( "Konexioa ixteko eskaera jasota." )
        elkarrizketa.close()
        exit( 0 )
s.close()

