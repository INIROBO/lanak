#!/usr/bin/env python3

import socket, sys, cv2, szasar, numpy

def iserror(message):
    '''
    Mezu bat emanda, errorea den adierazten du
    '''
    return message.startswith("ER-")

def geterror(message):
    '''
    Errore kodea itzultzen du errore mezu bat emanda
    '''
    if(message.startswith("ER-")):
        code = int(message[3:])
        return code
    else:
        return 0

def getFirst(encoded, decoder = "us-ascii", delimiter = "|"):
    result = ""
    for i in encoded:
        char = bytes([i]).decode(decoder)
        if char == delimiter:
            return result
        else:
            result += char
    return b""


def communicate(s, command, params, encoder = "us-ascii",
                decoder = "us-ascii", ender = True):
    '''
    Komando bat emanda eta parametro batzuk "|"-z zatituta,
    eskaera sortu, bidali eta erantzuna itzuliko du.
    Dekodetzailea eta kodeatzailea zein diren espezifikatu behar
    da. Kontrako kasuan us-ascii erabiliko da.
    '''
    eskaera1 = "{0}".format(command)

    eskaera2 = params

    eskaera3 = "{0}".format("|\r\n")


    if ender:
        eskaera = eskaera1.encode("us-ascii") +\
            eskaera2.encode(encoder)\
            + eskaera3.encode("us-ascii")
    else:
        eskaera = eskaera1.encode("us-ascii") +\
            eskaera2



    print(eskaera)
    s.sendall(eskaera)

    erantzuna = b""
    CRLF = '|\r\n'.encode("us-ascii")
    while True:
        buf = s.recv(1024)
        erantzuna += buf
        if erantzuna[len(erantzuna)-3:] == CRLF:
            break

    erantzunaCommand_dec = erantzuna[:3].decode("us-ascii")
    erantzunaParams_dec = erantzuna[3:len(erantzuna)-3].decode(decoder)
    if iserror(erantzunaCommand_dec):
        return "ER-", geterror(erantzunaParams_dec)
    else:
        return "OK+", erantzunaParams_dec

def auth(s, erabiltzailea, pasahitza):
    '''
    Autentikazio eskaera egiten da erabiltzaile eta pasahitzarekin
    eta erantzuna itzultzen du
    '''
    return communicate(s, "AUTH", "{0}|{1}".format(erabiltzailea, pasahitza), "utf-8", "us-ascii")


def quit(s):
    '''
    Zerbitzariarekin sesioa bukatzeko eskaera egiten da eta
    erantzuna itzuli.
    '''
    return communicate(s, "QUIT", "")


def lsus(s):
    '''
    Erabiltzaile zerrenda eskatzen du eta horren erantzuna
    itzuli.
    '''
    emaitza = communicate(s, "LSUS", "", decoder = "utf-8")
    if emaitza[0] == "OK+":
        erabiltzaileak = emaitza[1][:]
        zerrenda = erabiltzaileak.split('|')
        return "OK+", zerrenda
    else:
        return emaitza

def lsph(s, erabiltzailea = ""):
    '''
    Argazki zerrenda itzultzen du. Erabiltzailea zehazten ez bada,
    unekoa unekoaren irudiak lortuko dira, bestela, emandako erabiltzailearenak.
    '''
    emaitza = communicate(s, "LSPH", erabiltzailea, encoder = "utf-8")
    if emaitza[0] == "OK+":
        irudiak = emaitza[1]
        zerrenda = irudiak.split('|')
        return "OK+", zerrenda
    else:
        return emaitza

def pict(s, filename, size):
    '''
    Argazki bat igotzeko eskaera egiten du emandako fitxategi
    izenarekin eta tamainarekin. Erantzuna itzuliko du.
    '''

    return communicate(s, "PICT", "{0}|{1}".format( filename, size ), encoder = "utf-8")


def uplo(s, img):
    '''
    Emandako img argazkia igotzen du eta erantzuna itzuli.
    '''

    # img_open = cv2.imread(img, flags = -1)
    # img_str = numpy.array(cv2.imencode('.jpg', img_open)[1]).tostring()
    print(img)
    print("over")
    return communicate(s, "UPLO", img, ender = False)



def phot(s, id):
    '''
    Emandako identifikazioa duen argazkia jaisteko eskaera egiten du.
    Erantzuna positiboa bada irudia jaitsi eta itzuliko du,
    bestela errorea itzuliko du.
    '''

    eskaera = "PHOT{0}|\r\n".format( id )

    s.sendall(eskaera.encode("us-ascii"))

    img_encoded = b""
    erantzuna = b""
    size = 0
    CRLF = '|\r\n'.encode("us-ascii")
    while True:
        # Erantzuna jasotazen joan
        buf = s.recv(1024)
        erantzuna += buf
        erantzunaDec = getFirst(erantzuna)
        print(erantzunaDec)

        # CRLF dagoenean ezin da irudi bat jaso, errorea du izan beharra
        if erantzunaDec[:3] == "ER-":
            return "ER-", geterror(erantzunaDec)
        # Erantzunak parametroak badauzka, tamaina jaso dela esan nahi du
        if erantzunaDec:
            img_encoded = erantzuna[len(erantzunaDec)+1:]
            print(img_encoded)
            size = int(erantzunaDec[3:]) - len(img_encoded)
            print(size)
            break

    # Lortu irudia
    img_encoded += szasar.recvall(s, size)
    print(len(img_encoded))

    # Irudia deskodetu
    np_arr = numpy.frombuffer(img_encoded, dtype=numpy.uint8)
    img = cv2.imdecode(np_arr, flags = -1)

    return "OK+", img

