import socket, sys, os, functions

def menu():
	print(">Menu (Egin nahi denaren ondoko zenbakia idatzi bakarrik)")
	print("0 - Erabiltzaile zerrenda")
	print("1 - Argazkien zerrenda")
	print("2 - Argazkia jaitsi")
	print("3 - Argazkia partekatu")
	print("4 - Saioa itxi")
	print("? - Menu hau inprimatu")

PORT = 6012

if len( sys.argv ) != 2:
	print( "Erabiliera: {} <zerbitzaria".format( sys.argv[0] ) )
	exit ( 1 )
	
zerb_helb = (sys.argv[1], PORT)
#make socket
s = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
s.connect ( zerb_helb )
#static fields for login control
logged = false
username = null

while True:
	#automatically asks the user to log in if not logged in
	if (not logged):
		print( "Erabiltzailea:" )
		erabiltzailea = input()
		if not erabiltzailea:
			break
		print( "Pasahitza:" )
		pasahitza = input()
		if not pasahitza:
			break
		erantzuna = functions.login(s,erabiltzailea,pasahitza)
		if erantzuna[0] == "OK+":
			logged = true
			username = erabiltzailea
			print("Saioa hasi da.")
		else:
			break
	else:
		menu()
		
		komando = input()
		
		if komando == "0":
			erantzuna = functions.getUsers(s)
			if erantzuna[0] == "OK+":
			#TO DO: String-a irakurri, | bakoitza -> new line
			else:
				continue	
		elif komando == "1":
			print("Zein erabiltzaileren argazki zerrenda nahi duzu? (Argazki guztiak nahi badira, ezer idatzi gabe enter sakatu")
			eb = input()
			if not eb:
				lista = functions.getImageList(s)
			else
				lista = functions.getImageList(s, eb)
			if lista[0] == "OK+":
				#TO DO: Lista irakurri, print id + deskripzioa, | bakoitza -> new line
			else:
				continue
				
		elif komando == "2":
			print("Argazkiaren identifikadorea sartu, mesedez (hutsa atzera egiteko):")
			id = input()
			if not id:
				continue
			erantzuna = functions.download(s, id)
			if erantzuna[0] == "OK+":
			#TO DO: erantzuna interpretatu
			else:
				continue
		elif komando == "3":
			#TO DO
		elif komando == "4";
			erantzuna = functions.logout(s)
			if erantzuna[0] == "OK+":
				print("Saioa itxi da. Programa amaitzen.")
				break
			
		elif komando == "?":
			menu()
		else
			print("Hori ez da baliozko komando bat. Aseguratu hitzez hitz idatzi duzula.")
		
	
	
s.close()