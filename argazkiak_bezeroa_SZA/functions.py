import time, network, cv2, numpy

def errorea(num):
    '''
    Errore kode bat emanda, honen ondorioz inprimatu beharreko testua
    pantailaratzen du.
    '''
    if num == 1:
        print("Error 1: Espero ez zen komando bat jaso da.")
    if num == 2:
        print("Error 2: Komando ezezaguna.")
    if num == 3:
        print("Error 3: Espero ez zen parametroa. Parametro bat jaso da\
              espero ez zen tokian.")
    if num == 4:
        print("Error 4: Hautazkoa ez den parametro bat jaso da.")
    if num == 5:
        print("Error 5: Formatu egokia ez duen parametro bat jaso da.")
    if num == 6:
        print("Error 6: Ezin izan da sesioa ireki. Kautotze errore bat egon da.")
    if num == 7:
        print("Error 7: Zerbitzariak ezin izan du erabiltzaile zerrenda lortu.")
    if num == 8:
        print("Error 8: Zerbitzariak ezin izan du lortu argazki zerrenda.")
    if num == 9:
        print("Error 9: Zerbitzarian ez dago argazki horrentzako lekurik.")
    if num == 10:
        print("Error 10: Arazo bat egon da irudiaren bidalketarekin.")
    if num == 11:
        print("Error 11: Ez dago emandako identifikadorea duen argazkirik.")
    if num == 12:
        print("Error 12: Ezin izan da emandako identifikadorea duen argazkia \
              atzitu.")

    return "ER-", num

def login(s, erabiltzailea, pasahitza):
    '''
    Erabiltzaile bat emanda eta bere pasahitza, sesioa irekitzen saiatzen da.
    '''
    erantzuna = network.auth(s, erabiltzailea, pasahitza)

    if erantzuna[0] == "ER-":
        # Pixka bat itxaron eta berriro probatu
        time.sleep(2)
        erantzuna = network.auth(s, erabiltzailea, pasahitza)

        if erantzuna[0] == "ER-":
            errorea(erantzuna[1])

    return erantzuna

def logout(s):
    '''
    Sesioa ixten du.
    '''
    erantzuna = network.quit(s)

    if erantzuna[0] == "ER-":
        # Pixka bat itxaron eta berriro probatu
        time.sleep(2)
        erantzuna = network.quit(s)

        if erantzuna[0] == "ER-":
            errorea(erantzuna[1])

    return erantzuna

def getUsers(s):
    '''
    Erabiltzaileen zerrenda itzultzen du.
    '''
    erantzuna = network.lsus(s)

    if erantzuna[0] == "ER-":
        # Pixka bat itxaron eta berriro probatu
        time.sleep(2)
        erantzuna = network.lsus(s)

        if erantzuna[0] == "ER-":
            errorea(erantzuna[1])

    return erantzuna

def getImageList(s, erabiltzailea = ""):
    '''
    Irudien zerrenda itzultzen du. Erabiltzailea zehazten ez bada,
    unekoa unekoaren irudiak lortuko dira, bestela, emandako erabiltzailearenak.
    '''
    if erabiltzailea == "":
        erantzuna = network.lsph(s)
    else:
        erantzuna = network.lsph(s, erabiltzailea)

    if erantzuna[0] == "ER-":
        # Pixka bat itxaron eta berriro probatu
        time.sleep(2)

        if erabiltzailea == "":
            erantzuna = network.lsph(s)
        else:
            erantzuna = network.lsph(s, erabiltzailea)

        if erantzuna[0] == "ER-":
            errorea(erantzuna[1])

    return erantzuna

def upload(s, filename, file):
    '''
    Irudi izen bat, tamaina eta irudiaren edukia emanda, zerbitzarira igotzen
    saiatuko da.
    '''

    img_open = cv2.imread(file, flags = -1)
    img_str = numpy.array(cv2.imencode('.jpg', img_open)[1]).tostring()

    # Eskaera
    commandLength = len("UPLO".encode("us-ascii"))
    print("command length:", commandLength)
    erantzuna = network.pict(s, filename, len(img_str) +\
                            commandLength)
    if erantzuna[0] == "ER-":
        # pixka bat itxaron eta berriro probatu
        time.sleep(2)
        commandLength = len("UPLO".encode("us-ascii"))
        print("command length:", commandLength)
        erantzuna = network.pict(s, filename, len(img_str) +\
                                 commandLength)

        if erantzuna[0] == "ER-":
            errorea(erantzuna[1])
            return erantzuna

    # Igoera
    erantzuna2 = network.uplo(s, img_str)
    if erantzuna2[0] == "ER-":
        # pixka bat itxaron eta berriro probatu
        time.sleep(2)
        erantzuna2 = network.uplo(s, img_str)

        if erantzuna2[0] == "ER-":
            errorea(erantzuna2[1])
    return erantzuna2

def download(s, id, path):
    '''
    Irudi identifikadore bat emanik, hori duen irudia jaisten saiatuko da.
    '''

    erantzuna = network.phot(s, id)
    if erantzuna[0] == "ER-":
        # pixka bat itxaron eta berriro probatu
        time.sleep(2)
        erantzuna = network.phot(s, id)

        if erantzuna[0] == "ER-":
            errorea(erantzuna[1])

    cv2.imwrite(path, erantzuna[1])
    return erantzuna
