import network, sys, socket, functions, cv2
import numpy as np

PORT = 50004

if len( sys.argv ) != 2:
    print( "Erabiliera: {} <zerbitzaria".format( sys.argv[0] ) )
    exit ( 1 )

zerb_helb = (sys.argv[1], PORT)

s = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
s.connect ( zerb_helb )

print (functions.login(s, "kaixo", "kaixo"))
print (functions.getImageList(s, "kaixo"))
print(functions.getUsers(s))
print(functions.upload(s, "kaixo", "Figure_1.jpg"))
_, img = functions.download(s, 1, "test2.jpg")
# cv2.imwrite("test2.jpg", img)
print(functions.logout(s))
